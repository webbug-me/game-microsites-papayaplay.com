# Microsite for BlackShot on the papayaplay.com 

### Spec
> - AngularJS 1.5
> - SASS

### Link
> - [BlackShot Microsites](https://blackshot.papayaplay.com/bsglb.do?tp=update)

### Each folders 
```
cd /folders/package.json
```

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```
